import unittest
import os
import log_analyzer
import json

class TestLog_Analyzer(unittest.TestCase):
    def test_calc_config(self):
        self.cfg = log_analyzer.config
        with open("test_log_analyzer.json") as f_read_json:
            self.add_cfg = json.load(f_read_json)
        self.cfg = {**self.cfg, **self.add_cfg}
        self.assertEqual(self.cfg["REPORT_SIZE"], 500)
        self.assertEqual(self.cfg["REPORT_DIR"], "test1")
        self.assertEqual(self.cfg["LOG_DIR"], "test2")
        self.assertEqual(self.cfg["REP_TABLE_REPLACE"], "test4")
        self.assertEqual(self.cfg["MAX_ERROR_PERCENT"], 123)
        self.assertEqual(self.cfg["LOG_FILE_MASK"], "test5")

    def test_get_last_log_file(self):
        self.cfg = {}
        self.cfg["LOG_DIR"] = "test_logs"
        self.cfg["LOG_FILE_MASK"] = "^nginx-access-ui\\.log-(?P<date>\\d{8})(\\.gz)?$"
        self.cfg["LOG_DATE_MASK"] = "\\d{8}"
        self.cfg["LOG_DATE_FORMAT"] = "%Y%m%d"
        self.fileinfo = log_analyzer.get_last_log_file(self.cfg)
        self.assertTrue(os.path.exists(self.fileinfo.name))

    def test_get_records(self):
        self.test_get_last_log_file()
        self.cfg["MAX_ERROR_PERCENT"] = 30
        self.records = log_analyzer.get_records(self.fileinfo.name, self.cfg)
        self.grecords = log_analyzer.records_group(self.records)
        for x in self.grecords:
            x["count_perc"] = int(x["count_perc"])
            x["time_sum"] = int(x["time_sum"])
            x["time_perc"] = int(x["time_perc"])
            x["time_avg"] = int(x["time_avg"])
            x["time_max"] = int(x["time_max"])
            x["time_med"] = int(x["time_med"])

        self.assertEqual(self.grecords[0], {"url": "/test1", "count": 4, "count_perc": 40,
                         "time_sum": 14, "time_perc": 23, "time_avg": 3, "time_max": 5, "time_med": 4})
        self.assertEqual(self.grecords[1], {"url": "/test2", "count": 3, "count_perc": 30,
                         "time_sum": 18, "time_perc": 30, "time_avg": 6, "time_max": 7, "time_med": 6})
        self.assertEqual(self.grecords[2], {"url": "/test3", "count": 2, "count_perc": 20,
                         "time_sum": 17, "time_perc": 28, "time_avg": 8, "time_max": 9, "time_med": 9})
        self.assertEqual(self.grecords[3], {"url": "/test4", "count": 1, "count_perc": 10,
                         "time_sum": 10, "time_perc": 16, "time_avg": 10, "time_max": 10, "time_med": 10})


if __name__ == '__main__':
    unittest.main()

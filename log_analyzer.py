"""Домашнее задание 1"""
# !/usr/bin/env python
# -*- coding: utf-8 -*-

# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#            '$status $body_bytes_sent "$http_referer" '
#            '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#            '$request_time';

import os
import gzip
import datetime
import logging
import copy
import json
import re
import pathlib
import string
import argparse
import collections

LOG_FILE_MASK = "^nginx-access-ui\\.log-(?P<date>\\d{8})(\\.gz)?$"

config = {
    "REPORT_SIZE": 1000,                    # Максимальное количество URL-ов в отчете
    "REPORT_DIR": "reports",                # Папка с отчетами
    "LOG_DIR": "logs",                      # Папка с логами
    "LOG_DATE_MASK": "\\d{8}",               # Маска даты в файле лога
    "LOG_DATE_FORMAT": "%Y%m%d",            # Формат даты в имени файла лога
    "REP_TABLE_REPLACE": "$table_json",     # Заменяемое вхождение в шаблон отчета
    "MAX_ERROR_PERCENT": 30,                # % порога ошибок парсера
    "LOG_FILE": "logs\\log_analyzer.log",   # Файл логирования скрипт
    "DEF_CONFIG_PATH": "log_analyzer.json"  # Дефолтный путь для конфига
}

Fileinfo = collections.namedtuple("Fileinfo", "name date")


def get_file_config():
    """Получаем конфиг из файла"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', nargs='?', default='log_analyzer.json')
    config_file = parser.parse_args().config
    if not os.path.exists(config_file):
        raise Exception("Не найден файл конфига: " + config_file)
    with open(config_file) as f_read_json:
        json_data = json.load(f_read_json)
    return json_data


def get_last_log_file(aconfig):
    """Получаем последний файл лога"""
    last_file_date = None
    rfile = None
    rfile_date = None

    for file in os.listdir(aconfig["LOG_DIR"]):
        result = re.search(LOG_FILE_MASK, file)
        if not result:
            continue
        try:
            file_date = datetime.datetime.strptime(result.group(1), aconfig["LOG_DATE_FORMAT"])
        except Exception:
            logging.exception("Ошибка формата даты в файле лога")
            continue

        if (last_file_date is None) or (file_date > last_file_date):
            last_file_date = file_date
            rfile = file
            rfile_date = file_date

    if rfile is None:
        return Fileinfo(None, None)

    rfile = os.path.join(aconfig["LOG_DIR"], rfile)
    return Fileinfo(rfile, rfile_date)


def get_report_file_name(afile, aconfig):
    """Получаем имя файла отчета"""
    if not os.path.isdir(aconfig["REPORT_DIR"]):
        os.makedirs(aconfig["REPORT_DIR"])

    report_date_string = afile["file_date"].strftime("%Y.%m.%d")
    report_filename = "report-{}.html".format(report_date_string)
    report_filename = os.path.join(aconfig["REPORT_DIR"], report_filename)
    return report_filename


def log_reader(file_name):
    open_fn = gzip.open if file_name.endswith(".gz") else open
    with open_fn(file_name, mode='rb') as log_file:
        for line in log_file:
            yield line.decode("utf-8")


def get_records(file_name, aconfig):
    """Формируем список сочетаний URL, request_time"""
    result = []
    log_generator = log_reader(file_name)
    line_count = 0
    file_lines = 0
    for line in log_generator:
        file_lines = file_lines + 1
        fields = line.split()
        try:
            result.append([fields[6], float(fields[-1]), False])
            line_count = line_count + 1
        except Exception:
            # При возникновении ошибки при распарсивания строки просто не добавляем список в массив
            pass
    # Проверяем не превышен ли % ошибок
    if int(100 * (file_lines - line_count) / file_lines) > aconfig["MAX_ERROR_PERCENT"]:
        raise Exception("Превышен порог процента ошибок парсинга строк лога")

    # После сортировки списка по URL и $RequestTime
    # можно будет спокойно пробежаться по списку и сгруппировать данные по URL
    result.sort()
    return result


def records_group(records):
    """Формируем сгрупированный по URL список"""
    result = []
    last_url = ""
    rcount = 0
    rtime_sum = 0
    rall_count = len(records)
    rall_time_sum = 0
    rmax_time = 0
    r_index = 0
    r_first_url_index = 0

    def group_item_append():
        if (rcount % 2) == 0:
            median_index = r_first_url_index + int(rcount / 2)
        else:
            median_index = r_first_url_index + int((rcount-1) / 2)

        result.append({
            'url': last_url,
            'count': rcount,
            'count_perc': 0,
            'time_sum': rtime_sum,
            'time_perc': 0,
            'time_avg': 0,
            'time_max': rmax_time,
            'time_med': median_index})

    # Пробегаемся по нашему списку, отсортированному по URL и RequestTime
    for record in records:
        # Если сменился URL (включая вариант с последним элементом массива)
        # то добавляем сгруппированный элемент результата
        if (last_url and (not last_url == record[0])):
            group_item_append()

        # Если необходимо обнулить переменные
        # То есть, если это первый элемент списка или сменился URL
        if (not last_url) or (not last_url == record[0]):
            r_first_url_index = r_index
            last_url = record[0]
            rcount = 0
            rtime_sum = 0
            rmax_time = 0
        # Отрабатываем данные
        rcount = rcount + 1
        rtime_sum = rtime_sum + record[1]
        rall_time_sum = rall_time_sum + record[1]
        if (abs(record[1]) - rmax_time) > 0.000001:
            rmax_time = record[1]
        r_index = r_index + 1
    # Конец отработки последней группы
    group_item_append()

    # Пробегаемся по сгрупированному списку и уточняем данные
    for i in result:
        # Количество URL в % от общего числа
        i["count_perc"] = 100 * i["count"] / rall_count if not (rall_count == 0) else 0
        # Суммарный $request_time в % от общей суммы $request_time
        if abs(rall_time_sum) >= 0.000001:
            i["time_perc"] = 100 * i["time_sum"] / rall_time_sum
        else:
            i["time_perc"] = 0
        # Среднее $request_time для данного URL. i["count"] не может быть 0
        i["time_avg"] = i["time_sum"] / i["count"]
        # Ну и медианное значение
        if (i["time_med"] >= 0) and (i["time_med"] < len(records)):
            i["time_med"] = records[i["time_med"]][1]
        else:
            i["time_med"] = 0
    return result


def trunc_list(arecords, aconfig):
    """Оставляем только REPORT_SIZE URL-ов"""
    # Сначала сортируем
    result = sorted(arecords, key=lambda x: x["time_sum"], reverse=True)
    # А теперь отрезаем лишнее
    if len(result) > aconfig["REPORT_SIZE"]:
        result = result[:aconfig["REPORT_SIZE"]]
    return result


def out_report(arecords, afile_name):
    """Формируем отчет"""
    if arecords is None:
        arecords = []
    html_template = pathlib.Path(__file__).with_suffix(".report_template")
    # Загружаем данные в строку из шаблона
    with open(html_template, "r") as f_read_template:
        html_template_str = f_read_template.read()

    template = string.Template(html_template_str)
    rendered = template.safe_substitute(table_json=json.dumps(arecords))
    # Сохраняем полученный отчет
    with open(afile_name, 'w') as f_write_report:
        f_write_report.write(rendered)


def main():
    united_config = {**copy.copy(config), **get_file_config()}
    logging.basicConfig(filename=config.get("LOG_FILE"), level=logging.INFO,
                        format="[%(asctime)s] %(levelname).1s %(message)s", datefmt="%Y.%m.%d %H:%M:%S")
    try:
        logging.info("============== Старт скрипта ===========")
        logging.info("Получение последнего файла лога")
        fileinfo = get_last_log_file(united_config)

        if fileinfo.name is None:
            logging.info("Не обнаружен файл лога")
            return

        logging.info("Анализируемый файл лога: " + fileinfo.name)

        if not os.path.isdir(united_config["REPORT_DIR"]):
            os.makedirs(united_config["REPORT_DIR"])
        report_date_string = fileinfo.date.strftime("%Y.%m.%d")
        report_file_name = "report-{}.html".format(report_date_string)
        report_file_name = os.path.join(united_config["REPORT_DIR"], report_file_name)
        logging.info("Файл отчета: " + report_file_name)

        if os.path.exists(report_file_name):
            logging.warning("Данный файл лога уже обработан")
            return

        logging.info("Загрузка файла лога")
        records = get_records(fileinfo.name, united_config)
        logging.info("Группировка полученных данных по URL")
        grecords = records_group(records)

        logging.info("Обрезание полученных данных")
        grecords = trunc_list(grecords, united_config)
        logging.info("Формирование файла отчета")
        out_report(grecords, report_file_name)
        logging.info("Скрипт завершен успешно")

    except Exception as main_error:
        logging.exception("Фатальная ошибка")
        raise main_error


if __name__ == "__main__":
    main()
